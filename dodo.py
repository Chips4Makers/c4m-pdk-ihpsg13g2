# SPDX-License-Identifier: AGPL-3.0-or-later OR GPL-2.0-or-later OR CERN-OHL-S-2.0+ OR Apache-2.0
import os, yaml
from os.path import basename
from pathlib import Path
from textwrap import dedent
from xml.etree import ElementTree as ET
from dataclasses import dataclass
from typing import Tuple, Dict, Iterable, Collection

from doit.tools import create_folder

from pdkmaster.io.spice.doit import TaskManager as SpiceTaskManager
from pdkmaster.io.klayout.doit import TaskManager as KLayoutTaskManager
from pdkmaster.io.tasyag import LibertyCornerDataT, TaskManager as TasYagTaskManager
from pdkmaster.io.coriolis import TaskManager as CoriolisTaskManager


### Config

open_pdk_tasks = [
    "gds", "spice", "rtl", "liberty", "lef", "doc", "klayout", "coriolis",
]
DOIT_CONFIG = {
    "default_tasks": list(open_pdk_tasks),
}


### globals

from doitlib.common import *
os.environ["TOP_DIR"] = str(top_dir)

### cell list

cell_list_file = top_dir.joinpath("cell_list.yml")

def task_cell_list():
    """Regenerate cell list.

    This task is not run by default. It needs to be run manually when the cell list
    has been changed and then the updated file has to be commit to git.
    """
    def write_list():
        import yaml

        from c4m.pdk import ihpsg13g2

        # Generate layout for all cells so that possible extra cell are generated
        for lib in ihpsg13g2.libs:
            print(lib.name)
            for cell in lib.cells:
                print(f"  {cell.name}")
                cell.layout

        cell_list = {
            lib.name: list(cell.name for cell in lib.cells)
            for lib in ihpsg13g2.libs
        }
        with cell_list_file.open("w") as f:
            yaml.dump(cell_list, f)

    return {
        "title": lambda _: "Creating cell list file",
        "targets": (
            cell_list_file,
        ),
        "actions": (
            write_list,
        ),
    }

# We assume that the cell list is stored in git and is available in the top directory.
assert cell_list_file.exists()
with cell_list_file.open("r") as f:
    cell_list: Dict[str, Collection[str]]
    cell_list = yaml.safe_load(f)

iolib_name = "sg13g2_io"
os.environ["IOLIB_NAME"] = iolib_name
lib_deps = {
    "StdCellLib": (*pdkmaster_deps, *flexcell_deps),
    "StdCell3V3Lib": (*pdkmaster_deps, *flexcell_deps),
    iolib_name: (*pdkmaster_deps, *flexcell_deps, *flexio_deps),
}


### main tasks


#
# openpdk_tree and task
task_open_pdk_dir = openpdk_tree.task_func

def task_open_pdk():
    "All tasks to do for a completely fille open_pdk directory"
    return {
        "task_dep": open_pdk_tasks,
        "actions": (),
    }


#
# spice_models
openpdk_spice_dir = openpdk_tree.tool_dir(tool_name="ngspice")
spice_pdk_files = (
    "sg13g2_moslv_mod.lib", "sg13g2_moslv_parm.lib", "sg13g2_moslv_stat.lib", "cornerMOSlv.lib",
    "sg13g2_moshv_mod.lib", "sg13g2_moshv_parm.lib", "sg13g2_moshv_stat.lib", "cornerMOShv.lib",
    "resistors_mod.lib", "resistors_parm.lib", "resistors_stat.lib", "cornerRES.lib",
    "diodes.lib",
)
spice_models_all_lib = openpdk_spice_dir.joinpath("all.spice")
spice_models_tgts = (
    *(openpdk_spice_dir.joinpath(file) for file in spice_pdk_files),
    spice_models_all_lib,
)
def task_spice_models():
    "Copy and generate C4M version of the models"
    ihpsg13g2_pdk_spice_dir = ihpsg13g2_pdk_dir.joinpath("libs.tech", "ngspice", "models")

    def write_all():
        with spice_models_all_lib.open("w") as f:
            f.write(dedent("""
                * All corners file

                * lvmos
                .lib lvmos_tt
                .lib "cornerMOSlv.lib" mos_tt
                .endl

                .lib lvmos_ff
                .lib "cornerMOSlv.lib" mos_ff
                .endl

                .lib lvmos_ss
                .lib "cornerMOSlv.lib" mos_ss
                .endl

                .lib lvmos_fs
                .lib "cornerMOSlv.lib" mos_fs
                .endl

                .lib lvmos_sf
                .lib "cornerMOSlv.lib" mos_sf
                .endl

                * hvmos
                .lib hvmos_tt
                .lib "cornerMOShv.lib" mos_tt
                .endl

                .lib hvmos_ff
                .lib "cornerMOShv.lib" mos_ff
                .endl

                .lib hvmos_ss
                .lib "cornerMOShv.lib" mos_ss
                .endl

                .lib hvmos_fs
                .lib "cornerMOShv.lib" mos_fs
                .endl

                .lib hvmos_sf
                .lib "cornerMOShv.lib" mos_sf
                .endl

                * resistors
                .lib res_typ
                .lib "cornerRES.lib" res_typ
                .endl

                * resistors
                .lib res_bcs
                .lib "cornerRES.lib" res_bcs
                .endl

                * resistors
                .lib res_wcs
                .lib "cornerRES.lib" res_wcs
                .endl

                * diodes
                .lib dio
                .include "diodes.lib"
                .endl
            """[1:]))

    return {
        "file_dep": tuple(
            ihpsg13g2_pdk_spice_dir.joinpath(file) for file in spice_pdk_files
        ),
        "targets": spice_models_tgts,
        "actions": (
            (create_folder, (openpdk_spice_dir,)),
            *(
                f"cp {str(ihpsg13g2_pdk_spice_dir.joinpath(file))}"
                f" {str(openpdk_spice_dir.joinpath(file))}"
                for file in spice_pdk_files
            ),
            write_all,
        )
    }


#
# spice_models_python (copy inside python module)
python_models_dir = ihpsg13g2_local_dir.joinpath("models")
def _repl_dir(p: Path) -> Path:
    b = basename(str(p))
    return python_models_dir.joinpath(b)
python_models_srctgts = tuple(
    (file, _repl_dir(file))
    for file in spice_models_tgts
)
python_models_init_file = python_models_dir.joinpath("__init__.py")
python_models_deps = tuple(scr for (scr, _) in python_models_srctgts)
python_models_tgts = tuple(tgt for (_, tgt) in python_models_srctgts)
def task_spice_models_python():
    """Copy SPICE models inside pdk module

    This way they can be used by pyspicefactory without needing separate
    PDK install"""
    def write_init():
        with python_models_init_file.open("w") as f:
            f.write(dedent("""
                # Autogenerated module
            """[1:]))

    return {
        "file_dep": python_models_deps,
        "targets": (*python_models_tgts, python_models_init_file),
        "actions": (
            (create_folder, (python_models_dir,)),
            write_init,
            *(
                f"cp {str(python_models_deps[n])} {str(python_models_tgts[n])}"
                for n in range(len(python_models_tgts))
            )
        )
    }


#
# spice task manager and tasks
spice_tm = SpiceTaskManager(
    tech_cb=get_tech, lib4name_cb=lib_from_name, cell_list=cell_list,
    top_dir=top_dir, openpdk_tree=openpdk_tree, netlistfab_cb=get_netlistfab,
)

spice_export_task = spice_tm.create_export_task(
    extra_filedep=c4m_pdk_ihpsg13g2_deps, extra_filedep_lib=lib_deps,
)
task_spice = spice_export_task.task_func


#
# klayout task manager and tasks
klayout_tm = KLayoutTaskManager(
    tech_cb=get_tech, lib4name_cb=lib_from_name, cell_list=cell_list,
    top_dir=top_dir, openpdk_tree=openpdk_tree, gdslayers_cb=get_gdslayers,
    textgdslayers_cb=get_textgdslayers,
)


klayout_dir = openpdk_tree.tool_dir(tool_name="klayout")
klayout_tech_dir = klayout_dir.joinpath("tech", "C4M.IHPSG13G2")
def task_copy_lyp():
    """Copy the upstream layer properties file"""
    src = ihpsg13g2_pdk_dir.joinpath("libs.tech", "klayout", "tech", "sg13g2.lyp")
    tgt = klayout_tech_dir.joinpath("sg13g2.lyp")

    return {
        "title": lambda _: "Copy the upstream layer properties file",
        "file_dep": (
            src,
        ),
        "targets": (
            tgt,
        ),
        "actions": (
            (create_folder, (klayout_tech_dir,)),
            f"cp {str(src)} {str(tgt)}",
        ),
    }


# klayout export
klayout_export_task = klayout_tm.create_export_task(
    spice_params_cb=get_spiceparams, layerprops_filename="sg13g2.lyp",
    extra_filedep=(*c4m_pdk_ihpsg13g2_deps, *pdkmaster_deps),
    extra_taskdep=("copy_lyp",),
)
task_klayout = klayout_export_task.task_func


# gds export
klayout_gds_task = klayout_tm.create_gds_task(
    extra_filedep=c4m_pdk_ihpsg13g2_deps, extra_filedep_lib=lib_deps,
)
task_gds = klayout_gds_task.task_func_gds


# drc
def waive_warning(_: str, __: str, cat: ET.Element) -> bool:
    # Waive the warning rules
    assert cat.text is not None
    return ("[Warning]" in cat.text)

klayout_drc_task = klayout_tm.create_drc_task(waive_func=waive_warning)
task_drc = klayout_drc_task.task_func_drc
task_drccells = klayout_drc_task.task_func_drccells


# lvs
klayout_lvs_task = klayout_tm.create_lvs_task()
task_lvs = klayout_lvs_task.task_func_lvs
task_lvscells = klayout_lvs_task.task_func_lvscells


# sign-off
task_signoff = klayout_tm.task_func_signoff


#
# HiTAS/Yagle task manager and tasks
def tasyag_filter_celllist(items: Iterable[Tuple[str, Collection[str]]]) -> Iterable[Tuple[str, Collection[str]]]:
    for lib, cells in items:
        if lib.startswith("StdCell"):
            yield (lib, tuple(filter(lambda cell: cell != "Gallery", cells)))
tasyag_cell_list: Dict[str, Collection[str]] = dict(tasyag_filter_celllist(cell_list.items()))

tasyag_tm = TasYagTaskManager(
    tech_cb=get_tech, lib4name_cb=lib_from_name, cell_list=tasyag_cell_list,
    top_dir=top_dir, openpdk_tree=openpdk_tree,
    spice_models_dir=python_models_dir,
)


# extra SPICE init file
def task_rtl_spice_init_files():
    """Generate SPICE init files for RTL generation"""
    @dataclass
    class RTLInitSpiceInfo:
        name: str
        lv_corner: str
        hv_corner: str
        init_file: Path

    infos = (
        RTLInitSpiceInfo(
            name="rtl", lv_corner="lvmos_tt", hv_corner="hvmos_tt", init_file=tmp_dir.joinpath("init.spi"),
        ),
        *(
            RTLInitSpiceInfo(
                name=f"{lib}_nom", lv_corner="lvmos_tt", hv_corner="hvmos_tt",
                init_file=tmp_dir.joinpath(f"{lib}_nom", "init.spi"),
            )
            for lib in tasyag_cell_list.keys()
        ),
        *(
            RTLInitSpiceInfo(
                name=f"{lib}_slow", lv_corner="lvmos_ss", hv_corner="hvmos_ss",
                init_file=tmp_dir.joinpath(f"{lib}_slow", "init.spi"),
            )
            for lib in tasyag_cell_list.keys()
        ),
        *(
            RTLInitSpiceInfo(
                name=f"{lib}_fast", lv_corner="lvmos_ff", hv_corner="hvmos_ff",
                init_file=tmp_dir.joinpath(f"{lib}_fast", "init.spi"),
            )
            for lib in tasyag_cell_list.keys()
        ),
    )

    def gen_files(info: RTLInitSpiceInfo):
        d = info.init_file.parent
        d.mkdir(parents=True, exist_ok=True)

        with info.init_file.open("w") as f:
            f.write(dedent(f"""
                * RTL spice init
                .lib "{spice_models_all_lib}" {info.lv_corner}
                .lib "{spice_models_all_lib}" {info.hv_corner}
            """[1:]))

    for info in infos:
        d = info.init_file.parent
        yield {
            "title": (lambda _: f"Spice init model file for {info.name}"),
            "name": info.name,
            "task_dep": ("spice_models",),
            "targets": (info.init_file,),
            "actions": (
                (gen_files, (info,)),
            )
        }


# rtl
rtl_tm = tasyag_tm.create_rtl_task(
    work_dir=tmp_dir, override_dir=override_dir,
    spice_model_files=tmp_dir.joinpath("init.spi"),
    extra_taskdep=("spice_models_python",),
)
task_rtl = rtl_tm.task_func


# liberty
liberty_corner_data: LibertyCornerDataT = {
    "StdCellLib": {
        "nom": (1.80, 25, (tmp_dir.joinpath("StdCellLib_nom", "init.spi"),)),
        "fast": (1.98, -20, (tmp_dir.joinpath("StdCellLib_fast", "init.spi"),)),
        "slow": (1.62, 85, (tmp_dir.joinpath("StdCellLib_slow", "init.spi"),)),
    },
    "StdCell3V3Lib": {
        "nom": (3.30, 25, (tmp_dir.joinpath("StdCell3V3Lib_nom", "init.spi"),)),
        "fast": (3.63, -20, (tmp_dir.joinpath("StdCell3V3Lib_fast", "init.spi"),)),
        "slow": (2.97, 85, (tmp_dir.joinpath("StdCell3V3Lib_slow", "init.spi"),)),
    },
}

liberty_tm = tasyag_tm.create_liberty_task(
    work_dir=tmp_dir, corner_data=liberty_corner_data,
)
# This task will be used inside task_liberty() function
# task_liberty_tasyag = liberty_tm.task_func


#
# liberty
def task_liberty():
    # Create liberty files

    # Standard generation for standard cells
    yield from liberty_tm.task_func()

    # Custom generation for IO cells
    from doitlib import liberty as _lbrty # pyright: ignore
    liberty_io_dir = openpdk_tree.views_dir(lib_name=iolib_name, view_name="liberty")
    liberty_io_file = liberty_io_dir.joinpath(f"{iolib_name}_dummy.lib")


    yield {
        "name": iolib_name,
        "doc": f"Creating liberty file for {iolib_name}",
        "file_dep": (*c4m_pdk_ihpsg13g2_deps, *lib_deps[iolib_name], _lbrty.__file__),
        "targets": (liberty_io_file,),
        "actions": (
            (_lbrty.io, None, dict(libname=iolib_name)),
        )
    }


#
# LEF
def task_lef():
    """Generate LEF files"""
    # Currenlty only implemented ad-hoc for IO library
    from doitlib import lef # pyright: ignore

    lef_io_dir = openpdk_tree.views_dir(lib_name=iolib_name, view_name="lef")
    lef_io_file = lef_io_dir.joinpath(f"{iolib_name}.lef")
    lef_ionotrack_file = lef_io_dir.joinpath(f"{iolib_name}_notracks.lef")

    yield {
        "name": iolib_name,
        "doc": f"Creating lef file for {iolib_name}",
        "file_dep": (*c4m_pdk_ihpsg13g2_deps, *lib_deps[iolib_name], lef.__file__),
        "targets": (lef_io_file, lef_ionotrack_file),
        "actions": (
            (lef.generate, None, dict(libname=iolib_name)),
        )
    }


#
# coriolis
def task_coriolis():
    """Generate coriolis support files"""

    coriolis_dir = openpdk_tree.tool_dir(tool_name="coriolis")
    corio_dir = coriolis_dir.joinpath("techno", "etc", "coriolis2")
    corio_node130_dir = corio_dir.joinpath("node130")
    corio_ihpsg13g2_dir = corio_node130_dir.joinpath("ihpsg13g2")

    corio_nda_init_file = corio_dir.joinpath("__init__.py")
    corio_node130_init_file = corio_node130_dir.joinpath("__init__.py")
    corio_ihpsg13g2_init_file = corio_ihpsg13g2_dir.joinpath("__init__.py")
    corio_ihpsg13g2_techno_file = corio_ihpsg13g2_dir.joinpath("techno.py")
    corio_ihpsg13g2_lib_files = tuple(
        corio_ihpsg13g2_dir.joinpath(f"{lib}.py") for lib in cell_list.keys()
    )

    def gen_init():
        from c4m.pdk import ihpsg13g2

        with corio_ihpsg13g2_init_file.open("w") as f:
            print("from .techno import *", file=f)
            for lib in ihpsg13g2.libs:
                print(f"from .{lib.name} import setup as {lib.name}_setup", file=f)

            print(
                "\n__lib_setups__ = [{}]".format(
                    ",".join(f"{lib.name}.setup" for lib in ihpsg13g2.libs)
                ),
                file=f,
            )

    def gen_coriolis():
        from pdkmaster.io import coriolis as _iocorio
        from c4m.flexcell import coriolis_export_spec
        from c4m.pdk import ihpsg13g2

        expo = _iocorio.FileExporter(
            tech=ihpsg13g2.tech, gds_layers=ihpsg13g2.gds_layers,
            spec=coriolis_export_spec,
        )

        with corio_ihpsg13g2_techno_file.open("w") as f:
            f.write(dedent("""
                # Autogenerated file
                # SPDX-License-Identifier: GPL-2.0-or-later OR AGPL-3.0-or-later OR CERN-OHL-S-2.0+
            """))
            f.write(expo())

        for lib in ihpsg13g2.libs:
            with corio_ihpsg13g2_dir.joinpath(f"{lib.name}.py").open("w") as f:
                f.write(expo(lib))

    return {
        "title": lambda _: "Creating coriolis files",
        "file_dep": (
            *c4m_pdk_ihpsg13g2_deps,
            *pdkmaster_deps, *flexcell_deps, *flexio_deps, #*flexmem_deps,
        ),
        "targets": (
            corio_nda_init_file, corio_node130_init_file, corio_ihpsg13g2_init_file,
            corio_ihpsg13g2_techno_file, *corio_ihpsg13g2_lib_files,
        ),
        "actions": (
            (create_folder, (corio_ihpsg13g2_dir,)),
            corio_nda_init_file.touch, corio_node130_init_file.touch,
            gen_init, gen_coriolis,
        ),
    }


#
# docs
sim_dir = top_dir.joinpath("sim")
def task_doc():
    """Generate the docs"""
    from doitlib import iolib_readme as _rdme # pyright: ignore

    version_file = top_dir.joinpath("version.txt")

    yield {
        "name": "README",
        "doc": "Create README file",
        "file_dep": (_rdme.__file__, version_file),
        "targets": (
            iolib_readme_file,
        ),
        "actions": (
            (_rdme.generate, None, dict(libname=iolib_name)),
        )
    }

    drivestrength_sim_notebook = sim_dir.joinpath("SimOutDriveStrength.ipynb")
    drivestrength_sim_out_file = sim_dir.joinpath("SimOutDriveStrength.html")
    drivestrength_doc_file = iolib_doc_dir.joinpath("DriveStrengthSim.html")
    yield {
        "name": "DriveStrength",
        "doc": "Simulate and document output drive strength",
        "file_dep": (
            *c4m_pdk_ihpsg13g2_deps,
            drivestrength_sim_notebook,
        ),
        "targets": (
            drivestrength_doc_file,
        ),
        "actions": (
            (create_folder, (iolib_doc_dir,)),
            f"cd {str(sim_dir)}; jupyter nbconvert --to html --execute {str(drivestrength_sim_notebook)}",
            f"cp {str(drivestrength_sim_out_file)} {str(drivestrength_doc_file)}",
        ),
    }

    input_sim_notebook = sim_dir.joinpath("SimInputPerformance.ipynb")
    input_sim_out_file = sim_dir.joinpath("SimInputPerformance.html")
    input_doc_file = iolib_doc_dir.joinpath("InputPerformance.html")
    yield {
        "name": "InputPerformance",
        "doc": "Simulate and document input performance",
        "file_dep": (
            *c4m_pdk_ihpsg13g2_deps,
            input_sim_notebook,
        ),
        "targets": (
            input_doc_file,
        ),
        "actions": (
            (create_folder, (iolib_doc_dir,)),
            f"cd {str(sim_dir)}; jupyter nbconvert --to html --execute {str(input_sim_notebook)}",
            f"cp {str(input_sim_out_file)} {str(input_doc_file)}",
        ),
    }


#
# release
def task_tarball():
    """Create a tarball"""
    from datetime import datetime

    tarballs_dir = top_dir.joinpath("tarballs")
    t = datetime.now()
    tarball = tarballs_dir.joinpath(f'{t.strftime("%Y%m%d_%H%M")}_openpdk_c4m_ihpsg13g2.tgz')

    return {
        "title": lambda _: "Create release tarball",
        "task_dep": (
            *open_pdk_tasks,
        ),
        "targets": (tarball,),
        "actions": (
            (create_folder, (tarballs_dir,)),
            f"cd {str(top_dir)}; tar czf {str(tarball)} open_pdk",
        )
    }
def task_tarball_nodep():
    """Create a tarball from existing open_pdk"""
    from datetime import datetime

    tarballs_dir = top_dir.joinpath("tarballs")
    t = datetime.now()
    tarball = tarballs_dir.joinpath(f'{t.strftime("%Y%m%d_%H%M")}_nd_openpdk_c4m_ihpsg13g2.tgz')

    return {
        "title": lambda _: "Create release tarball",
        "targets": (tarball,),
        "actions": (
            (create_folder, (tarballs_dir,)),
            f"cd {str(top_dir)}; tar czf {str(tarball)} open_pdk",
        )
    }


#
# patch for upstream PDK
def task_patch4upstream():
    """Create a patch for the upstream PDK"""
    script = top_dir.joinpath("upstream", "mkpatch.sh")
    return {
        "title": lambda _: "Create patch for upstream",
        "file_dep": (
            script,
        ),
        "uptodate": [False], # Always rerun
        "task_dep": (
            *open_pdk_tasks,
        ),
        "actions": (
            str(script),
        )
    }
def task_patch4upstream_nodep():
    """Create a patch for the upstream PDK"""
    script = top_dir.joinpath("upstream", "mkpatch.sh")
    return {
        "title": lambda _: "Create patch for upstream",
        "file_dep": (
            script,
        ),
        "uptodate": [False], # Always rerun
        "actions": (
            str(script),
        )
    }


#
# Copy open_pdk into upstream repo
coriolis_tm = CoriolisTaskManager(
    tech_cb=get_tech, lib4name_cb=lib_from_name, cell_list=cell_list,
    top_dir=top_dir, openpdk_tree=openpdk_tree,
)

coriolis_cppdk_task = coriolis_tm.create_upstreampdk_task(
    alliancectk_co_dir=top_dir.joinpath("upstream", "alliance-check-toolkit"), openpdk_task="open_pdk",
)
task_coriolis_cp_pdk = coriolis_cppdk_task.task_func
