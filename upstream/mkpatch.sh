#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later OR GPL-2.0-or-later OR CERN-OHL-S-2.0+ OR Apache-2.0
upstream_dir=$TOP_DIR/upstream/IHP-Open-PDK/ihp-sg13g2
openpdk_dir=$TOP_DIR/open_pdk/C4M.IHPSG13G2

if [ ! -d $upstream_dir ];
then
  echo "git submodule 'upstream/IHP-Open-PDK' is not initialized"
  exit 20
fi

# Get latest upstream main branch
cd $TOP_DIR
git submodule update --remote upstream/IHP-Open-PDK

# Switch to own branch
cd $upstream_dir/libs.ref
git checkout -B upstream_patch remotes/origin/dev

# Replace files with latest version
cd $IOLIB_NAME
rm -fr doc gds lef liberty spice
cp -r $openpdk_dir/libs.ref/$IOLIB_NAME/* .
cd ..
git add $IOLIB_NAME

# Commit
git commit -m "Patch for upstream"
