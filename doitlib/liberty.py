# SPDX-License-Identifier: AGPL-3.0-or-later OR GPL-2.0-or-later OR CERN-OHL-S-2.0+ OR Apache-2.0
from textwrap import indent, dedent
from datetime import datetime
from typing import Optional

from c4m import flexio as _io
from c4m.pdk import ihpsg13g2

from .common import *


def io(*, libname: str):
    assert libname == "sg13g2_io" # only IO library supported

    liberty_io_dir = openpdk_tree.views_dir(lib_name=libname, view_name="liberty")
    liberty_io_dir.mkdir(parents=True, exist_ok=True)
    liberty_io_file = liberty_io_dir.joinpath(f"{libname}_dummy.lib")

    def inputpin_str(*, name: str, ispad: bool=False, domain: str):
        if domain == "core":
            vss = "vss"
            vdd = "vdd"
        elif domain == "io":
            vss = "iovss"
            vdd = "iovdd"
        else:
            assert False, f"Internal error: unhandled domain '{domain}'"

        s = dedent(f"""
            pin ({name}) {{
                direction : input;
        """[1:])
        if ispad:
            s += "    is_pad : true;\n"
        s += dedent(f"""
                input_voltage : {domain}_input;
                related_ground_pin : {vss};
                related_power_pin : {vdd};
                max_transition : 200;
                capacitance : 15.0;
                rise_capacitance : 15.0;
                rise_capacitance_range (12.0, 15.0);
                fall_capacitance : 15.0;
                fall_capacitance_range (12.0, 15.0);
            }}
        """[1:])

        return s

    def outputpin_str(*,
        name: str, ispad: bool=False, related: str, tri: Optional[str]=None, inout: bool=False,
        domain: str,
    ):
        if domain == "core":
            vss = "vss"
            vdd = "vdd"
        elif domain == "io":
            vss = "iovss"
            vdd = "iovdd"
        else:
            assert False, f"Internal error: unhandled domain '{domain}'"

        s = dedent(f"""
            pin ({name}) {{
                direction : {"output" if not inout else "inout"};
                function : "({related})";
        """[1:])
        if tri is not None:
            s += f'    three_state: "({tri})";\n'
        if ispad:
            s += f"    is_pad : true;\n"
        s += indent(dedent(f"""
                output_voltage : {domain}_output;
                related_ground_pin : {vss};
                related_power_pin : {vdd};
                max_capacitance : 500;
                max_transition : 200;
                timing () {{
                    related_pin : "{related}";
                    timing_type : combinational;
                    cell_rise (delay_template_2x2) {{
                        values ( \\
                            "1000, 1000", \\
                            "1000, 1000" \\
                        );
                    }}
                    rise_transition (delay_template_2x2) {{
                        values ( \\
                            "200, 200", \\
                            "200, 200" \\
                        );
                    }}
                    cell_fall (delay_template_2x2) {{
                        values ( \\
                            "1000, 1000", \\
                            "1000, 1000" \\
                        );
                    }}
                    fall_transition (delay_template_2x2) {{
                        values ( \\
                            "200, 200", \\
                            "200, 200" \\
                        );
                    }}
                }}
        """[1:]), "    ")
        if tri is not None:
            s += indent(dedent(f"""
                timing () {{
                    related_pin : "{tri}";
                    timing_sense : negative_unate;
                    timing_type : three_state_enable;
                    cell_rise (delay_template_2x2) {{
                        values ( \\
                            "1000, 1000", \\
                            "1000, 1000" \\
                        );
                    }}
                    rise_transition (delay_template_2x2) {{
                        values ( \\
                            "200, 200", \\
                            "200, 200" \\
                        );
                    }}
                    cell_fall (delay_template_2x2) {{
                        values ( \\
                            "1000, 1000", \\
                            "1000, 1000" \\
                        );
                    }}
                    fall_transition (delay_template_2x2) {{
                        values ( \\
                            "200, 200", \\
                            "200, 200" \\
                        );
                    }}
                }}
                timing () {{
                    related_pin : "{tri}";
                    timing_sense : positive_unate;
                    timing_type : three_state_disable;
                    cell_rise (delay_template_2x2) {{
                        values ( \\
                            "1000, 1000", \\
                            "1000, 1000" \\
                        );
                    }}
                    rise_transition (delay_template_2x2) {{
                        values ( \\
                            "200, 200", \\
                            "200, 200" \\
                        );
                    }}
                    cell_fall (delay_template_2x2) {{
                        values ( \\
                            "1000, 1000", \\
                            "1000, 1000" \\
                        );
                    }}
                    fall_transition (delay_template_2x2) {{
                        values ( \\
                            "200, 200", \\
                            "200, 200" \\
                        );
                    }}
                }}
            """[1:]), "    ")
        s += "}\n"

        return s

    with liberty_io_file.open(mode="w") as f:
        f.write(dedent(f"""
            /* Autogenerated file; please don't edit.
            date: {datetime.now()}
            */

            /************************************************************************

            Copyright 2024 IHP PDK Authors

            Licensed under the Apache License, Version 2.0 (the "License");
            you may not use this file except in compliance with the License.
            You may obtain a copy of the License at

                https://www.apache.org/licenses/LICENSE-2.0

            Unless required by applicable law or agreed to in writing, software
            distributed under the License is distributed on an "AS IS" BASIS,
            WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
            See the License for the specific language governing permissions and
            limitations under the License. 

            ************************************************************************/

            library ({libname}_dummy) {{
                delay_model : table_lookup;
                capacitive_load_unit (1,ff);
                current_unit : "1mA";
                leakage_power_unit : "1mW";
                pulling_resistance_unit : "1kohm";
                time_unit : "1ps";
                voltage_unit : "1V";
                voltage_map (vss, 0.0);
                voltage_map (vdd, 1.2);
                voltage_map (iovss, 0.0);
                voltage_map (iovdd, 3.3);
                default_cell_leakage_power : 0;
                default_fanout_load : 1;
                default_inout_pin_cap : 1;
                default_input_pin_cap : 1;
                default_leakage_power_density : 0;
                default_max_fanout : 30;
                default_max_transition : 5000;
                default_output_pin_cap : 0;
                in_place_swap_mode : match_footprint;
                input_threshold_pct_fall : 50;
                input_threshold_pct_rise : 50;
                nom_process : 1;
                nom_temperature : 25;
                nom_voltage : 1.8;
                output_threshold_pct_fall : 50;
                output_threshold_pct_rise : 50;
                slew_derate_from_library : 1;
                slew_lower_threshold_pct_fall : 20;
                slew_lower_threshold_pct_rise : 20;
                slew_upper_threshold_pct_fall : 80;
                slew_upper_threshold_pct_rise : 80;
                operating_conditions ("typ") {{
                    process : 1;
                    temperature : 25;
                    voltage : 1.8;
                    tree_type: "balanced_tree";
                }}
                default_operating_conditions : "typ";
                input_voltage (core_input) {{
                    vih : 1.2;
                    vil : 0;
                    vimax : 1.2;
                    vimin : 0;
                }}
                output_voltage (core_output) {{
                    voh : 1.2;
                    vol : 0;
                    vomax : 1.2;
                    vomin : 0;
                }}
                input_voltage (io_input) {{
                    vih : 3.3;
                    vil : 0;
                    vimax : 3.3;
                    vimin : 0;
                }}
                output_voltage (io_output) {{
                    voh : 3.3;
                    vol : 0;
                    vomax : 3.3;
                    vomin : 0;
                }}
                lu_table_template (delay_template_2x2) {{
                    variable_1 : input_net_transition;
                    variable_2 : total_output_net_capacitance;
                    index_1 ("10, 200");
                    index_2 ("500, 30000");
                }}
        """[1:]))

        # Be sure that the cells exist
        ihpsg13g2.iolib.cells["sg13g2_Gallery"].circuit

        for cell in ihpsg13g2.iolib.cells:
            if isinstance(cell, (
                _io.PadInT, _io.PadOutT, _io.PadTriOutT, _io.PadInOutT,
                _io.PadVssT, _io.PadVddT, _io.PadIOVssT, _io.PadIOVddT,
                _io.FillerT, _io.CornerT,
            )):
                bb = cell.layout.boundary
                assert bb is not None
                area = round(bb.area)
                s = indent(dedent(f"""
                    cell ({cell.name}) {{
                        area: {area};
                        dont_touch : true;
                        dont_use : true;
                        timing_model_type : abstracted;
                        pad_cell : true;
                        pg_pin (vss) {{
                            pg_type : primary_ground;
                            voltage_name : "vss";
                """), "    ")
                if isinstance(cell, _io.PadVssT):
                    s += "            is_pad : true;\n"
                s += indent(dedent(f"""
                        }}
                        pg_pin (vdd) {{
                            pg_type : primary_power;
                            voltage_name : "vdd";
                """[1:]), "        ")
                if isinstance(cell, _io.PadVddT):
                    s += "            is_pad : true;\n"
                s += indent(dedent(f"""
                        }}
                        pg_pin (iovss) {{
                            pg_type : primary_ground;
                            voltage_name : "iovss";
                """[1:]), "        ")
                if isinstance(cell, _io.PadIOVssT):
                    s += "            is_pad : true;\n"
                s += indent(dedent(f"""
                        }}
                        pg_pin (iovdd) {{
                            pg_type : primary_power;
                            voltage_name : "iovdd";
                """[1:]), "        ")
                if isinstance(cell, _io.PadIOVddT):
                    s += "            is_pad : true;\n"
                s += "        }\n"
                if isinstance(cell, _io.PadInT):
                    s += indent(
                        (
                            outputpin_str(name="p2c", related="pad", domain="core")
                            + inputpin_str(name="pad", ispad=True, domain="io")
                        ),
                        "        "
                    )
                elif isinstance(cell, _io.PadOutT):
                    s += indent(
                        (
                            inputpin_str(name="c2p", domain="core")
                            + outputpin_str(name="pad", ispad=True, related="c2p", domain="io")
                        ),
                        "        "
                    )
                elif isinstance(cell, _io.PadTriOutT):
                    s += indent(
                        (
                            inputpin_str(name="c2p", domain="core")
                            + inputpin_str(name="c2p_en", domain="core")
                            + outputpin_str(
                                name="pad", ispad=True, related="c2p", tri="c2p_en",
                                domain="io",
                            )
                        ),
                        "        "
                    )
                elif isinstance(cell, _io.PadInOutT):
                    s += indent(
                        (
                            outputpin_str(name="p2c", related="pad", domain="core")
                            + inputpin_str(name="c2p", domain="core")
                            + inputpin_str(name="c2p_en", domain="core")
                            + outputpin_str(
                                name="pad", ispad=True, related="c2p", tri="c2p_en",
                                inout=True, domain="io",
                            )
                        ),
                        "        "
                    )
                elif isinstance(cell, (
                    _io.PadVssT, _io.PadVddT, _io.PadIOVssT, _io.PadIOVddT,
                    _io.FillerT, _io.CornerT,
                )):
                    # No extra pins
                    pass
                else:
                    assert False, "Internal error"
                s += "    }\n"
                f.write(s)
        f.write("}\n")
