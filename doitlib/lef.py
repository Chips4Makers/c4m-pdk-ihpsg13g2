#!/bin/env python3
# SPDX-License-Identifier: AGPL-3.0-or-later OR GPL-2.0-or-later OR CERN-OHL-S-2.0+ OR Apache-2.0
from typing import List, Dict, cast
from datetime import datetime
from textwrap import dedent

from pdkmaster.technology import geometry as _geo, primitive as _prm
from pdkmaster.design import layout as _lay, circuit as _ckt

from c4m.flexio import IOSpecification, IOFrameSpecification
from c4m.pdk.ihpsg13g2 import tech, ihpsg13g2_ioframespec, ihpsg13g2_iofab
ihpsg13g2_ioframespec: IOFrameSpecification

from .common import *

lef_io_cells = (
    "Corner", "Filler200", "Filler400", "Filler1000", "Filler2000", "Filler4000", "Filler10000",
    "IOPadIn",
    "IOPadOut4mA", "IOPadOut16mA", "IOPadOut30mA",
    "IOPadTriOut4mA", "IOPadTriOut16mA", "IOPadTriOut30mA",
    "IOPadInOut4mA", "IOPadInOut16mA", "IOPadInOut30mA",
    "IOPadAnalog",
    "IOPadIOVss", "IOPadIOVdd", "IOPadVss", "IOPadVdd",
)

track_metals = ("Metal3", "Metal4", "Metal5", "TopMetal1", "TopMetal2")
pin_metals = ("Metal1", "Metal2", *track_metals)

dir_lookup: Dict[str, str] = {
    "iovss": "INOUT",
    "iovdd": "INOUT",
    "vss": "INOUT",
    "vdd": "INOUT",
    "pad": "INOUT",
    "padres": "INOUT",
    "p2c": "OUTPUT",
    "c2p": "INPUT",
    "c2p_en": "INPUT",
}
use_lookup: Dict[str, str] = {
    "iovss": "GROUND",
    "iovdd": "POWER",
    "vss": "GROUND",
    "vdd": "POWER",
    "pad": "SIGNAL",
    "padres": "SIGNAL",
    "p2c": "SIGNAL",
    "c2p": "SIGNAL",
    "c2p_en": "SIGNAL",
}

def rect_str(*, r: _geo.RectangularT):
    return f"RECT {r.left:.3f} {r.bottom:.3f} {r.right:.3f} {r.top:.3f}"

def trackobs_shapes(*,
    prims: _prm.Primitives, iospec: IOSpecification, framespec: IOFrameSpecification,
) -> Dict[str, _geo.Rect]:
    iovddvss_trackspec = framespec._track_specs_dict["vddvss"]
    track_top = iovddvss_trackspec.top

    def metalshape(metal: str) -> _geo.Rect:
        mprim = cast(_prm.MetalWire, prims[metal])
        tsspace = framespec.tracksegment_space
        space = tsspace.get(mprim, tsspace[None])
        top = track_top - 0.5*space
        return _geo.Rect(
            left=0.0, bottom=0.0, right=iospec.monocell_width, top=top
        )

    return {
        metal: metalshape(metal=metal)
        for metal in track_metals
    }

def obs_str(*, layout: _lay.LayoutT, obs_shapes: Dict[str, _geo.RectangularT]):
    bnd = layout.boundary
    assert bnd is not None
    s = "  OBS\n"
    s += "".join(
        f"    LAYER {l} ;\n"
        f"      {rect_str(r=obs_shapes.get(l, bnd))} ;\n"
        for l in (*pin_metals, "Via1", "Via2")
    )
    s += "  END\n"

    return s

def pin_str(*,
    net: _ckt.CircuitNetT, add_tracks: bool, tracks_only: bool,
    layout: _lay.LayoutT, bnd: _geo.RectangularT,
) -> str:
    s = (
        f"  PIN {net.name}\n"
        f"    DIRECTION {dir_lookup[net.name]} ;\n"
        f"    USE {use_lookup[net.name]} ;\n"
    )

    # Sort the shapes
    # track_shapes is of the DC tracks in the IO cell
    pin_shapes: Dict[str, List[_geo.Rect]] = {n: [] for n in pin_metals}
    # track_shapes is of the DC tracks in the IO cell
    track_shapes: Dict[str, List[_geo.Rect]] = {n: [] for n in pin_metals}
    # pad_shapes is for pad pin e.g. the pins touching the bottom of the cell
    pad_shapes: Dict[str, List[_geo.Rect]] = {n: [] for n in pin_metals}
    for ms in layout.filter_polygons(net=net, split=True, depth=0):
        mask = ms.mask
        shape = ms.shape

        if ms.mask.name.endswith(".pin"):
            assert isinstance(shape, _geo.Rect)

            layer_name = mask.name[:-4]
            assert layer_name in pin_metals
            if (not tracks_only) and (shape.top > (bnd.top - _geo.epsilon)):
                pin_shapes[layer_name].append(shape)
            elif (not tracks_only) and (shape.bottom < (bnd.bottom + _geo.epsilon)):
                pad_shapes[layer_name].append(shape)
            else:
                track_shapes[layer_name].append(shape)

    if any(pin_shapes.values()):
        s += "    PORT\n"
        for metal_name in pin_metals:
            shapes = pin_shapes[metal_name]
            if shapes:
                s += f"      LAYER {metal_name} ;\n"
                for shape in shapes:
                    s += f"        {rect_str(r=shape)} ;\n"
        s += "    END\n"

    if any(pad_shapes.values()):
        s += "    PORT\n"
        for metal_name in pin_metals:
            shapes = pad_shapes[metal_name]
            if shapes:
                s += f"      LAYER {metal_name} ;\n"
                for shape in shapes:
                    s += f"        {rect_str(r=shape)} ;\n"
        s += "    END\n"

    if add_tracks:
        for metal_name in pin_metals:
            shapes = track_shapes[metal_name]
            if shapes:
                s += "".join(
                    "    PORT\n"
                    f"      LAYER {metal_name} ;\n"
                    f"        {rect_str(r=shape)} ;\n"
                    "    END\n"
                    for shape in shapes
                )

    s += f"  END {net.name}\n"

    return s

def generate(*, libname: str):
    assert libname == "sg13g2_io" # only IO library supported

    lef_io_dir = openpdk_tree.views_dir(lib_name=libname, view_name="lef")
    lef_io_dir.mkdir(parents=True, exist_ok=True)
    lef_io_file = lef_io_dir.joinpath(f"{libname}.lef")
    lef_ionotrack_file = lef_io_dir.joinpath(f"{libname}_notracks.lef")

    monocell_obsshapes = trackobs_shapes(
        prims=tech.primitives, iospec=ihpsg13g2_iofab.spec, framespec=ihpsg13g2_ioframespec,
    )

    topmetal2 = cast(_prm.MetalWire, tech.primitives["TopMetal2"])
    site_name = f"{libname}Site"
    for file, add_tracks in (
        (lef_io_file, True),
        (lef_ionotrack_file, False),
    ):
        with file.open(mode="w") as f:
            f.write(dedent(
                f"""
                # Autogenerated file; please don't edit
                # date: {datetime.now()}

                ########################################################################
                #
                # Copyright 2024 IHP PDK Authors
                # 
                # Licensed under the Apache License, Version 2.0 (the "License");
                # you may not use this file except in compliance with the License.
                # You may obtain a copy of the License at
                # 
                #    https://www.apache.org/licenses/LICENSE-2.0
                # 
                # Unless required by applicable law or agreed to in writing, software
                # distributed under the License is distributed on an "AS IS" BASIS,
                # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
                # See the License for the specific language governing permissions and
                # limitations under the License.
                #
                ########################################################################

                VERSION 5.7 ;

                SITE {site_name}
                    CLASS PAD ;
                    SYMMETRY R90 ;
                    SIZE 1.00 BY {ihpsg13g2_ioframespec.cell_height:.2f} ;
                END {site_name}
                """[1:]
            ))

            class_lookup: Dict[str, str] = {
                "Corner": "SPACER",
                "Filler200": "SPACER",
                "Filler400": "SPACER",
                "Filler1000": "SPACER",
                "Filler2000": "SPACER",
                "Filler4000": "SPACER",
                "Filler10000": "SPACER",
                "IOPadIn": "INPUT",
                "IOPadOut4mA": "OUTPUT",
                "IOPadOut16mA": "OUTPUT",
                "IOPadOut30mA": "OUTPUT",
                "IOPadTriOut4mA": "OUTPUT",
                "IOPadTriOut16mA": "OUTPUT",
                "IOPadTriOut30mA": "OUTPUT",
                "IOPadInOut4mA": "INOUT",
                "IOPadInOut16mA": "INOUT",
                "IOPadInOut30mA": "INOUT",
                "IOPadAnalog": "INOUT",
                "IOPadIOVss": "POWER",
                "IOPadIOVdd": "POWER",
                "IOPadVss": "POWER",
                "IOPadVdd": "POWER",
            }
            for cell_name_base in lef_io_cells:
                cell = ihpsg13g2_iofab.get_cell(cell_name_base)
                ckt = cell.circuit
                layout = cell.layout
                tracks_only = (cell_name_base.startswith("Corner") or cell_name_base.startswith("Filler"))
                nets = ckt.nets

                bnd = cell.layout.boundary
                assert bnd is not None

                # header
                # Avoid -0.000 in output
                o_left = 0.0 if abs(bnd.left) < _geo.epsilon else -bnd.left
                o_bottom = 0.0 if abs(bnd.bottom) < _geo.epsilon else - bnd.bottom
                f.write(dedent(
                    f"""
                    MACRO {cell.name}
                        CLASS PAD {class_lookup[cell_name_base]} ;
                        ORIGIN {o_left:.3f} {o_bottom:.3f} ;
                        FOREIGN {cell.name} {bnd.left:.3f} {bnd.bottom:.3f} ;
                        SIZE {bnd.width:.3f} BY {bnd.height:.3f} ;
                        SYMMETRY X Y R90 ;
                        SITE {site_name} ;
                    """
                ))

                # pins
                for pin_name in sorted(port.name for port in ckt.ports):
                    net = nets[pin_name]
                    f.write(pin_str(
                        net=net, add_tracks=add_tracks, tracks_only=tracks_only,
                        layout=layout, bnd=bnd))

                # OBS
                if cell_name_base != "Corner":
                    obs_shapes = cast(Dict[str, _geo.RectangularT], monocell_obsshapes)
                    obs_shapes["TopMetal2"] = cell.layout.bounds(mask=topmetal2.mask)
                else:
                    obs_shapes = {
                        metal: cast(_geo.RectangularT, cell.layout.boundary)
                        for metal in track_metals
                    }
                f.write(obs_str(layout=cell.layout, obs_shapes=obs_shapes))
                f.write(f"END {cell.name}\n")
