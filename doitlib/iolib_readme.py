#!/bin/env python3
# SPDX-License-Identifier: AGPL-3.0-or-later OR GPL-2.0-or-later OR CERN-OHL-S-2.0+ OR Apache-2.0
"Generate the README for upstream to IHP-Open-PDK"
from textwrap import dedent

from .common import *

def generate(*, libname: str):
    assert libname == "sg13g2_io"

    iolib_doc_dir.mkdir(parents=True, exist_ok=True)

    with iolib_readme_file.open("w") as f:
        f.write(dedent(f"""
            This is the {libname} library. The following files are included in this library:

            * `doc`:
              * `README.md`: this file
              * `InputPerformance.html`: simulation results for input bandwidth and duty ratio.
              * `DriveStrengthSim.html`: simulation of drive strength of the output drivers.
            * `gds/sg13g2_io.gds`: GDS view of the IO cells
            * `spice/sg13g2_io.spi`: spice netlists of the IO cells
            * `lef/sg13g2_io.lef`: LEF view of the IO cells
            * `liberty/sg13g2_io_dummy.lib`: dummy liberty view of the IO cells.
              This file only contains enough information to get the OpenROAD flow going; no timing
              or power data is available in this file.

            These files are generated from python scripts of the Chips4Makers based IHP SG13G2
            PDK. The code can be found in the
            [c4m-pdk-ihpsg13g2](https://gitlab.com/Chips4Makers/c4m-pdk-ihpsg13g2.git) repo.
            This library is built from version `{get_version()}` of that source code.
            The `README.md` file of this project explains how to use the code in there. The whole
            build of the files plus preparation of the files described above for upstreaming can be
            generated with the command `pdm doit patch4upstream`.

            It also contains externally contributed files:

            * cdl/sg13g2_iocell.cdl: CDL netlist
            * verilog/sg13g2_io.v: verilog netlist
            * liberty/sg13g2_iocell_*lib: liberty files with timing
          """[1:]))
