# SPDX-License-Identifier: AGPL-3.0-or-later OR GPL-2.0-or-later OR CERN-OHL-S-2.0+ OR Apache-2.0
import site
from pathlib import Path
from typing import Optional

import pdkmaster.technology, pdkmaster.design, pdkmaster.dispatch
import pdkmaster.io.spice, pdkmaster.io.klayout
import c4m, c4m.flexcell, c4m.flexio

from pdkmaster.typing import GDSLayerSpecDict
from pdkmaster.task import OpenPDKTree
from pdkmaster.technology import technology_ as _tch
from pdkmaster.design import library as _lbry
from pdkmaster.io.spice import spice_ as _iospi

### support functions

def first(it):
    return next(iter(it))


_version: Optional[str] = None
def get_version() -> str:
    global _version

    if _version is None:
        with open(f"{top_dir}/version.txt") as f:
            _version = next(iter(f))[:-1]

        try:
            idx = _version.index("+")
        except:
            pass
        else:
            _version = _version[:idx]

    return _version


def get_tech() -> _tch.Technology:
    from c4m.pdk import ihpsg13g2
    return ihpsg13g2.tech
def lib_from_name(libname: str) -> _lbry.Library:
    from c4m.pdk import ihpsg13g2

    if libname == "sg13g2_io":
        lib_varname = "iolib"
    else:
        lib_varname = libname.lower()
    return getattr(ihpsg13g2, lib_varname)
def get_netlistfab() -> _iospi.SpiceNetlistFactory:
    from c4m.pdk import ihpsg13g2
    return ihpsg13g2.netlistfab
def get_spiceparams() -> _iospi.SpicePrimsParamSpec:
    from c4m.pdk import ihpsg13g2
    return ihpsg13g2.prims_spiceparams
def get_gdslayers() -> GDSLayerSpecDict:
    from c4m.pdk import ihpsg13g2
    return ihpsg13g2.gds_layers
def get_textgdslayers() -> GDSLayerSpecDict:
    from c4m.pdk import ihpsg13g2
    return ihpsg13g2.textgds_layers


### variables


top_dir = Path(__file__).parents[1]
open_pdk_dir = top_dir.joinpath("open_pdk")
openpdk_tree = OpenPDKTree(top=open_pdk_dir, pdk_name="C4M.IHPSG13G2")

dist_dir = top_dir.joinpath("dist")
override_dir = top_dir.joinpath("override")
tmp_dir = top_dir.joinpath("tmp")

c4m_local_dir = top_dir.joinpath("c4m")
ihpsg13g2_local_dir = c4m_local_dir.joinpath("pdk", "ihpsg13g2")
c4m_inst_dir = Path(site.getsitepackages()[0]).joinpath("c4m")
ihpsg13g2_inst_dir = c4m_inst_dir.joinpath("pdk", "ihpsg13g2")
flexcell_inst_dir = Path(first(c4m.flexcell.__path__))
flexio_inst_dir = Path(first(c4m.flexio.__path__))

c4m_pdk_ihpsg13g2_deps = (
    *ihpsg13g2_local_dir.rglob("*.py"),
)
pdkmaster_deps = (
    *Path(first(pdkmaster.technology.__path__)).rglob("*.py"),
    *Path(first(pdkmaster.design.__path__)).rglob("*.py"),
    *Path(first(pdkmaster.dispatch.__path__)).rglob("*.py"),
)
pdkmaster_io_spice_deps = (
    *Path(first(pdkmaster.io.spice.__path__)).rglob("*.py"),
)
pdkmaster_io_klayout_deps = (
    *Path(first(pdkmaster.io.klayout.__path__)).rglob("*.py"),
)
flexcell_deps = (
    *Path(first(c4m.flexcell.__path__)).rglob("*.py"),
)
flexio_deps = (
    *Path(first(c4m.flexio.__path__)).rglob("*.py"),
)

ihpsg13g2_pdk_dir = top_dir.joinpath("deps", "IHP-Open-PDK", "ihp-sg13g2")

iolib_doc_dir = openpdk_tree.views_dir(lib_name="sg13g2_io", view_name="doc")
iolib_readme_file = iolib_doc_dir.joinpath("README.md")
